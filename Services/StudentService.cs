﻿using System.Collections.Generic;
using Example_04.Models;
using Example_04.Repository;
namespace Example_04.Services
{
    public class StudentService : IStudent
    {
        private readonly IStudentRepository _studentRepository;
        public StudentService(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public IEnumerable<Student> Get()
        {
            return _studentRepository.Get();
            
        }
        public Student GetById(int? id)
        {
            return _studentRepository.GetById(id);
        }
        public void Insert(Student student)
        {
           _studentRepository.Insert(student);
            
          
        }
        public void Delete(int id)
        {
            _studentRepository.Delete(id);
            
        }
        public void Update(Student student)
        {
            _studentRepository.Update(student);
            
        }
        public void Save()
        {
           _studentRepository.Save();
        }
    }
    
}