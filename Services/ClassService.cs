﻿using System.Collections.Generic;
using Example_04.Models;
using Example_04.Repository;

namespace Example_04.Services
{
    public  class ClassService : IClass
    {
        private readonly IClassRepository _classRepository;
        public ClassService(IClassRepository classRepository)
        {
            _classRepository = classRepository;
        }
        public IEnumerable<Class> Get()
        {
             return _classRepository.Get();       
        }
        public Class GetById(int? id)
        {
            return _classRepository.GetById(id);
        }
        public void Insert(Class classes)
        {
           _classRepository.Insert(classes);
          
        }
        public void Delete(int id)
        {
            _classRepository.Delete(id);
           
        }
        public void Update(Class classes)
        {
            _classRepository.Update(classes);
           
        }
        public void Save()
        {
           _classRepository.Save();
        }
    }
}